const qr = require("qr-image");

const generate = async request => {
  const { url } = await request.json();
  const qr_png = qr.imageSync(url || "https://ctoon.party", {
    parse_url: true
  });
  return new Response(qr_png, { "Content-Type": "image/png" });
};

async function handleRequest(request) {
  let response;
  if (request.method === "POST") {
    response = await generate(request);
  } else {
    // no json in GET so make a null one
    response = await generate({
      json: () => {
        return { url: null };
      }
    });
  }
  return response;
}

addEventListener("fetch", event => {
  event.respondWith(handleRequest(event.request));
});
